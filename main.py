#import des fichiers/variables necessaire
from cinema.Film import *
from cinema.Acteur import *
from cinema.Historique import *
from cinema.Commande import *

tarif = 11
liste_commande = []

def menu():
    print("1: Enregistrer des commandes")
    print("2: Afficher l'historique des commandes")
    print("0: Quitter")
    choix = int(input("Que voulez-vous faire ? "))
    return choix

choix = menu()

while choix != 0:

    if choix == 1:
        

        print("Liste des films :")
        nbFilms = len(liste_film)

        for i in range(nbFilms):
            print((i+1),": ",liste_film[i].titre)

        numFilm = int(input("Quel film ? ")) - 1
        nbTickets = int(input("Combien de tickets ? "))
        prix_ticket = nbTickets * tarif
        commande = Commande(liste_film[numFilm], nbTickets, prix_ticket)
        addHistorique(liste_commande,commande)
        print("Vous avez choisi : ", liste_film[numFilm].titre, " avec ", liste_film[numFilm].acteurs," de la catégorie : ",liste_film[numFilm].categorie)
        print("Cela vous fera : ", prix_ticket, "euros")
        choix = menu()

    if choix == 2:
        getHistorique(liste_commande)
        choix = menu()