""" fonction permettant de recuperer l'historique ou d'y ajouter une entrée:
"""

#cette fonction permet d'ajouter une commande à l'historique
def addHistorique(liste_commande, commande):
    liste_commande.append(commande)

#cette fonction permet d'afficher la liste des commandes
def getHistorique(liste_commande):
#on definit le nombre  de film en fonction de la liste déjà établie
    nb_film = len(liste_commande)
    print ("Voici l'historique des commandes")
#on boucle sur notre tableau 
    for i in range(nb_film):
        print("Commande numéro : ",i+1)
        print (liste_commande[i].film.titre)
        print("Nb de tickets : ",liste_commande[i].nombres_ticket)
        print("Prix du ticket : ",liste_commande[i].prix_ticket)
        print("")
