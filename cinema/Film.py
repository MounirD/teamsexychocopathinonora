class Film:
""" Classe définissant un film par son:
-titre
-acteurs
-categorie
"""

    def __init__(self,titre,acteurs,categorie): #Constructeur
        self.titre = titre
        self.acteurs = acteurs
        self.categorie = categorie

#ajout de film films pour tester 
avengeur = Film("Avengeur","patinho_bg","humour")
la_naissance_du_mal = Film("la_naissance_du_mal","ryan_goslin","hitlerisation")

#on stocke les films dans une liste d'objet
liste_film = [avengeur,la_naissance_du_mal]